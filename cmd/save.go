/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"codeberg.org/h7c/savecli/app"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// saveCmd represents the save command
var saveCmd = &cobra.Command{
	Use:   "save",
	Short: "Save bookmarks list to remote Git repository",
	// 	Long: `A longer description that spans multiple lines and likely contains examples
	// and usage of using your command. For example:

	// Cobra is a CLI library for Go that empowers applications.
	// This application is a tool to generate the needed files
	// to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("saving and pushing bookmarks changes...")
		if err := app.Save(); err != nil {
			logrus.Fatalf("error: cannot save bookmark: %s", err)
		}
		fmt.Println("done! \\o/")
	},
}

func init() {
	rootCmd.AddCommand(saveCmd)
}
